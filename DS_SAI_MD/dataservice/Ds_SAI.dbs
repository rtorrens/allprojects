<data name="Ds_SAI" serviceNamespace="http://dss.dimacofi.cl/sai">
    <config id="DS_SAI">
        <property name="carbon_datasource_name">SAI_PROD</property>
    </config>
    <query id="qContratos" useConfig="DS_SAI">
        <sql>SELECT NRO_CONTRATO,RUT_CLIENTE,CLIENTE,DECODE(ESTADO,'VALIDADO','ACTIVO','INACTIVO') ESTADO 
    		FROM SAI_LISTA_CONTRATO_SC_V 
    	 WHERE RUT_CLIENTE = :RUT_CLIENTE
    	   AND ESTADO IN ('VALIDADO','FINALIZADO','FINIQUITADO')
    	</sql>
        <result defaultNamespace="http://sai.dimacofi.cl" element="Contracts" rowName="Contract">
            <element column="NRO_CONTRATO" name="contract_number" xsdType="string"/>
            <element column="RUT_CLIENTE" name="customer_number" xsdType="string"/>
            <element column="CLIENTE" name="customer" xsdType="string"/>
            <element column="ESTADO" name="status" xsdType="string"/>
        </result>
        <param name="RUT_CLIENTE" sqlType="STRING"/>
    </query>
    <operation name="getContracts">
        <call-query href="qContratos">
            <with-param name="RUT_CLIENTE" query-param="RUT_CLIENTE"/>
        </call-query>
    </operation>
    <query id="qContratosH" useConfig="DS_SAI">
        <sql>call Xxd_mw_ws_utils.getContractsH(?,?)</sql>
        <result defaultNamespace="http://sai.dimacofi.cl" element="Contracts" rowName="Contract">
            <element column="NRO_CONTRATO" name="contract_number" xsdType="string"/>
            <element column="RUT_CLIENTE" name="customer_number" xsdType="string"/>
            <element column="CLIENTE" name="customer" xsdType="string"/>
            <element column="ESTADO" name="status" xsdType="string"/>
        </result>
        <param name="RUT" sqlType="STRING"/>
        <param name="data" sqlType="ORACLE_REF_CURSOR" type="OUT"/>
    </query>
    <operation name="getContractsH">
        <call-query href="qContratosH">
            <with-param name="RUT" query-param="RUT"/>
        </call-query>
    </operation>
    <query id="qSummaryCounters" useConfig="DS_SAI">
        <sql>SELECT DISTINCT Periodo period
			  FROM Sai_Summary_Clicks_md_V
			 WHERE 1 = 1 
			    AND Rut = :RUT_CLIENTE
			    AND Nro_Contrato = :NRO_CONTRATO
			    ORDER BY TO_DATE(Periodo,'mm-yyyy') DESC
    	</sql>
        <properties>
            <property name="maxRows">2</property>
        </properties>
        <result defaultNamespace="http://sai.dimacofi.cl" element="summary">
            <element name="detail">
                <element column="period" name="period" xsdType="string"/>
            </element>
        </result>
        <param name="RUT_CLIENTE" sqlType="STRING"/>
        <param name="NRO_CONTRATO" sqlType="STRING"/>
    </query>
    <operation name="getSummaryCounters">
        <call-query href="qSummaryCounters">
            <with-param name="RUT_CLIENTE" query-param="RUT_CLIENTE"/>
            <with-param name="NRO_CONTRATO" query-param="NRO_CONTRATO"/>
        </call-query>
    </operation>
    <query id="qSummaryClicks" useConfig="DS_SAI">
        <sql>SELECT Nro_Factura Invoice,
			       Contador    Counter_Type,
			       Consumo     Quantity,
			       Porc        Percentage
			  FROM Sai_Summary_Clicks_md_V
			 WHERE 1 = 1 
			    AND to_Char(Nro_Contrato) = :NRO_CONTRATO
			    AND Periodo = :PERIOD
    	</sql>
        <result defaultNamespace="http://sai.dimacofi.cl" element="detail" rowName="data">
            <element column="Invoice" name="invoice" xsdType="string"/>
            <element column="Counter_Type" name="counter_type" xsdType="string"/>
            <element column="Quantity" name="quantity" xsdType="integer"/>
            <element column="Percentage" name="percentage" xsdType="float"/>
        </result>
        <param name="NRO_CONTRATO" sqlType="STRING"/>
        <param name="PERIOD" sqlType="STRING"/>
    </query>
    <operation name="getSummaryClicks">
        <call-query href="qSummaryCounters">
            <with-param name="NRO_CONTRATO" query-param="NRO_CONTRATO"/>
        </call-query>
    </operation>
    <operation name="getDetailsClicks">
        <call-query href="qSummaryClicks">
            <with-param name="NRO_CONTRATO" query-param="NRO_CONTRATO"/>
            <with-param name="PERIOD" query-param="PERIOD"/>
        </call-query>
    </operation>
    <query id="qSumCounters" useConfig="DS_SAI">
        <sql>SELECT DISTINCT Periodo period
			  FROM Sai_Summary_Clicks_md_V
			 WHERE 1 = 1 
			    AND Rut = :RUT_CLIENTE
			    AND Nro_Contrato = :NRO_CONTRATO
			    ORDER BY TO_DATE(Periodo,'mm-yyyy') DESC
    	</sql>
        <properties>
            <property name="maxRows">6</property>
        </properties>
        <result defaultNamespace="http://sai.dimacofi.cl" element="summary">
            <element name="detail">
                <element column="period" name="period" xsdType="string"/>
                <call-query href="qSumClicks">
                    <with-param column="NRO_CONTRATO" name="NRO_CONTRATO"/>
                    <with-param column="PERIOD" name="PERIOD"/>
                </call-query>
            </element>
        </result>
        <param name="RUT_CLIENTE" sqlType="STRING"/>
        <param name="NRO_CONTRATO" sqlType="STRING"/>
    </query>
    <query id="qSumClicks" useConfig="DS_SAI">
        <sql>SELECT Nro_Factura Invoice,
			       Contador    Counter_Type,
			       Consumo     Quantity,
			       Porc        Percentage
			  FROM Sai_Summary_Clicks_md_V
			 WHERE 1 = 1 
			    AND Nro_Contrato = :NRO_CONTRATO
			    AND Periodo = :PERIOD
    	</sql>
        <result defaultNamespace="http://sai.dimacofi.cl">
            <element name="data">
                <element column="Invoice" name="invoice" xsdType="string"/>
                <element column="Counter_Type" name="counter_type" xsdType="string"/>
                <element column="Quantity" name="quantity" xsdType="integer"/>
                <element column="Percentage" name="percentage" xsdType="float"/>
            </element>
        </result>
        <param name="NRO_CONTRATO" sqlType="STRING"/>
        <param name="PERIOD" sqlType="STRING"/>
    </query>
    <operation name="getSumCounters">
        <call-query href="qSumCounters">
            <with-param name="RUT_CLIENTE" query-param="RUT_CLIENTE"/>
            <with-param name="NRO_CONTRATO" query-param="NRO_CONTRATO"/>
        </call-query>
    </operation>
    <operation name="getSumClicks">
        <call-query href="qSumCounters">
            <with-param name="NRO_CONTRATO" query-param="NRO_CONTRATO"/>
        </call-query>
    </operation>
    
    <query id="q_SearchInvoice" useConfig="DS_SAI">
        <sql>call Xxd_mw_ws_utils.SearchInvoice(?,?,?,?,?,?,?)</sql>
        <result defaultNamespace="http://sai.dimacofi.cl" element="Invoices" rowName="Invoice">
            <element column="CUSTOMER_TRX_ID" name="customer_trx_id" xsdType="string"/>
            <element column="CUSTOMER_ID" name="customer_id" xsdType="string"/>
            <element column="CUSTOMER_NUMBER" name="rut" xsdType="string"/>
            <element column="TRX_NUMBER" name="trx_number" xsdType="string"/>
            <element column="TRX_DATE" name="trx_date" xsdType="string"/>
            <element column="DUE_DATE" name="due_date" xsdType="string"/>
            <element column="CT_REFERENCE" name="contrat" xsdType="string"/>
            <element column="AMOUNT_REMAINING" name="ammount_remaining" xsdType="string"/>
            <element column="AMOUNT" name="amount" xsdType="string"/>
        </result>
        <param name="RUT" sqlType="STRING"/>
        <param name="INVOICE_NUMBER" sqlType="STRING"/>
        <param name="INVOICE_RUT" sqlType="STRING"/>
        <param name="INVOICE_CONTRATO" sqlType="STRING"/>
        <param name="INVOICE_FROM" sqlType="STRING"/>
        <param name="INVOICE_TO" sqlType="STRING"/>
        <param name="data" sqlType="ORACLE_REF_CURSOR" type="OUT"/>
    </query>
    <operation name="SearchInvoice">
        <call-query href="q_SearchInvoice">
            <with-param name="RUT" query-param="RUT"/>
            <with-param name="INVOICE_NUMBER" query-param="INVOICE_NUMBER"/>
            <with-param name="INVOICE_RUT" query-param="INVOICE_RUT"/>
            <with-param name="INVOICE_CONTRATO" query-param="INVOICE_CONTRATO"/>
            <with-param name="INVOICE_FROM" query-param="INVOICE_FROM"/>
            <with-param name="INVOICE_TO" query-param="INVOICE_TO"/>

        </call-query>
    </operation>
    
    
    <query id="qClientesPreferentes" useConfig="DS_SAI">
        <sql>call Xxd_mw_ws_utils.getClientesPreferentes(?,?)</sql>
        <result defaultNamespace="http://sai.dimacofi.cl" element="Clientes" rowName="Cliente">
            <element column="RUT_CLIENTE" name="customer_number" xsdType="string"/>
            <element column="STATUS" name="status" xsdType="string"/>
        </result>
        <param name="RUT_CLIENTE" sqlType="STRING"/>
        <param name="data" sqlType="ORACLE_REF_CURSOR" type="OUT"/>
    </query>
    
    <operation name="ClientesPreferentes">
        <call-query href="qClientesPreferentes">
            <with-param name="RUT_CLIENTE" query-param="RUT_CLIENTE"/>
        </call-query>
    </operation>
</data>
